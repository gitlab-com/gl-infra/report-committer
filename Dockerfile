FROM ubuntu:focal

ENV DEBIAN_FRONTEND=noninteractive
RUN apt update && apt install -y git curl openssh-client gettext && apt -y clean
RUN git config --global user.email "no@mail"; git config --global user.name "Report Committer"
