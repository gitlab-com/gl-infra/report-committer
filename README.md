# Report Committer

### What does it do?

If you include the report committer template in your `.gitlab-ci.yml`, it will commit the contents of any job artifacts back to the repo.

### How to set it up?

You need to either:

 - create a project deploy key with write access
 - create a dedicated user and set up project permissions and SSH keys

Once you did that, make the SSH private key (in RSA format) available to the project via the `CI_DEPLOY_KEY` environment variable as a file.

Include the following lines into your `.gitlab-ci.yml` file:

```yaml
include:
  - 'https://gitlab.com/gitlab-com/gl-infra/report-committer/raw/master/include.yml'
```

Done. Have fun!
